import org.junit.Test;
import org.junit.runner.RunWith;

import static org.hamcrest.CoreMatchers.is;
import static org.junit.Assert.*;

public class ProgramTest {

    @Test
    public void testMovie() {
        Movie movie = new Movie("MovieTitle");

        assertThat(movie.getTitle(), is("MovieTitle"));
    }

    @Test
    public void testRental() {
        Movie movie = new Movie("MovieTitle");
        Rental rental = new Rental(movie, 2);

        assertThat(rental.getMovie(), is(movie));
        assertThat(rental.getDaysRented(), is(2));
    }

    @Test
    public void testCustomer() {
        Customer customer = new Customer("Name");

        assertThat(customer.getName(), is("Name"));
    }

    @Test
    public void testProgram() {
        Customer customer = new Customer("joe");
        Movie m1 = new Movie("movie1");
        Movie m2 = new ChildrensMovie("movie2");
        Movie m3 = new NewReleaseMovie("movie3");
        Rental r1 = new Rental(m1, 10);
        Rental r2 = new Rental(m2, 5);
        Rental r3 = new Rental(m3, 1);

        customer.addRental(r1);
        customer.addRental(r2);
        customer.addRental(r3);

        String expected = "Rental Record for joe\n" +
                "\tTitle\t\tDays\tAmount\n" +
                "\tmovie1\t\t10\t14.0\n" +
                "\tmovie2\t\t5\t4.5\n" +
                "\tmovie3\t\t1\t3.0\n" +
                "Amount owed is 21.5\n" +
                "You earned 3 frequent renter points";

        String result = customer.statement();
        assertThat(result, is(expected));
    }
}
