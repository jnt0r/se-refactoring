import java.lang.*;
import java.util.*;

public class Program
{

    public static void main(String[] args)
    {
        System.out.println("Welcome to the Movie Store");

        Movie m1 = new Movie("movie1");
        Movie m2 = new ChildrensMovie("movie2");

        Rental r1 = new Rental(m1, 10);
        Rental r2 = new Rental(m2, 5);

        Customer c1 = new Customer("joe");
        c1.addRental(r1);
        c1.addRental(r2);

        System.out.println("Let's get the Statement");
        System.out.println(c1.statement());
    }
}


