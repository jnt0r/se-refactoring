
import java.lang.*;
import java.util.*;

class Customer {
    private String name;
    private List<Rental> rentals = new ArrayList<>();

    public Customer (String name){
        this.name = name;
    };

    public String statement() {
        String result = "Rental Record for " + this.getName() + "\n";
        result += "\t" + "Title" + "\t" + "\t" + "Days" + "\t" + "Amount" + "\n";

        for (Rental rental: rentals) {
            result += "\t" + rental.getMovie().getTitle() + "\t" + "\t" + rental.getDaysRented() + "\t" + String.valueOf(rental.getAmount()) + "\n";
        }

        //add footer lines
        result += "Amount owed is " + String.valueOf(getTotalAmount()) + "\n";
        result += "You earned " + String.valueOf(getFrequentRenterPoints()) + " frequent renter points";
        return result;
    }

    private int getFrequentRenterPoints() {
        int result = 0;

        for (Rental rental: rentals) {
            result += rental.getFrequentRenterPoints();
        }

        return result;
    }

    private double getTotalAmount() {
        double result = 0;

        for (Rental rental: rentals) {
            result += rental.getAmount();
        }

        return result;
    }

    public void addRental(Rental arg) {
        rentals.add(arg);
    };

    public String getName (){
        return name;
    };

}
    