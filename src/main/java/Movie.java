public class Movie {
    private String title;

    public Movie(String title) {
        this.title = title;
    }

    public String getTitle() {
        return title;
    }

    double getAmount(int daysRented) {
        if (daysRented > 2)
            return 2 + (daysRented - 2) * 1.5;
        return 2;
    }

    int getRenterPoints(int daysRented) {
        return 1;
    }
}

